function parse(value) {
  return value.split(/ /);
}

function processStacks(valStack, opStack) {
  var op, v1, v2
  while (opStack.length > 0 && valStack.length >= 2) {
    [op, v2, v1] = [opStack.pop(), valStack.pop(), valStack.pop()]
    valStack.push(calculate(op, v1, v2))
  }
}

function evaluate(elements) {
  var valStack = [],
    opStack = []
  elements.forEach((e) => {
    switch (true) {
      case /\d+/.test(e):
        valStack.push(+e) // cast to number
        if (opStack.length && /[*/]/.test(opStack[opStack.length - 1])) {
          processStacks(valStack, opStack)
        }
        break
      case /[+*/-]/.test(e): // note: dash has to be last in regex character class
        opStack.push(e)
        break
      default:
        throw "Unhandled element: " + e
    }
  })
  processStacks(valStack, opStack)
  if (valStack.length != 1) throw "Empty stack!"
  if (opStack.length != 0) throw "Operator missing values: " + opStack
  return valStack.pop()
}

function calculate(op, v1, v2) {
  switch (op) {
    case "+":
      return v1 + v2;
    case "-":
      return v1 - v2;
    case "*":
      return v1 * v2;
    case "/":
      return v1 / v2;
    default:
      throw "Unrecognised operator: " + op
  }
}

class Calculator {
  eval(value) {
    return evaluate(parse(value))
  }
}

module.exports = Calculator
